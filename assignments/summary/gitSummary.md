## Git
# What is Git?

- Git is version-control software that makes collaboration with teammates super simple.
- Git lets you easily keep track of every revision you and your team make during the development of your software

# Vocabulary

- **Repository**: It is the collection of files and folders (code files) that you’re using git to track.
- **GitLab**: Remote storage solution for git repos.
- **Commit**: It’s like you’re taking picture/snapshot of the files as they exist at that moment. The commit will only exist on your local machine until it is pushed to a remote repository.
- **Push**: Pushing is essentially syncing your commits to GitLab.
- **Branch**: These are separate instances of the code that is different from the main codebase.
- **Merge**: When a branch is free of bugs and ready to become part of the primary codebase, it will get merged into the master branch.
- **Clone**: It takes the entire online repository and makes an exact copy of it on your local machine.
- **Fork**: Instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.


# Git Internals

Three main states that your files can reside in:

![Gitlab pointers](/assignments/summary/states of file in git.png)

# Git Commands

| Command | Purpose |
| ------ | ------ |
| `$ git clone <link-to-repository>` | Clone a repository into a new directory |
| `$ git checkout -b <your-branch-name>` | Switch branches or restore working tree files |
| `$ git add .` | Add file contents to the index |
| `$ git commit -sv`  | Record changes to the repository |
| `$ git push origin <branch-name>` | Update remote refs along with associated objects |






 
